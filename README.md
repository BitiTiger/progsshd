# ProgSSHD

This is a SSH server that runs a specified program on all clients without any
authentication. It is best used for status boards or trolling random script
kiddies.

![screenshot1](./img/img1.png)
A normal demo of ProgSSHD

![screenshot2](./img/img2.png)
Using ProgSSHD to Rick Roll clients

# Warnings

This program may have potential security issues. Be careful if using this in
production. It is recommended to use a virtual environment like a Docker
container.

A couple security issues:

- The server will accept *all* connections. No password, key, or valid username
  is required to connect.
- If this server hosts a real shell (like bash), every client will have a shell
  running on the same user as the SSH server.

# Usage

The server can be used with the command `./progsshd`.

The default settings are:

- Address: `localhost`
- Port: `22`
- Program: `./testsh`

All arguments can be viewed using `./progsshd --help` or the table below.

| Argument                    | Description                 |
| :-------------------------- | :-------------------------- |
| `-b` or `--binding=BINDING` | Set the address to bind.    |
| `-d` or `--dsakey=FILE`     | Set the dsa key.            |
| `-e` or `--executable=FILE` | Set the program to execute. |
| `-k` or `--hostkey=FILE`    | Set the host key.           |
| `-p` or `--port=PORT`       | Set the port to bind.       |
| `-r` or `--rsakey=FILE`     | Set the rsa key.            |
| `-v` or `--verbose`         | Get verbose output.         |
| `-?` or `--help`            | Give this help list         |
| `--usage`                   | Give a short usage message  |

# Building

1. Clone this repository:
`git clone https://gitlab.com/caton101/progsshd.git`

2. Enter the repository directory:
`cd progsshd`

3. Generate keys and compile code:
`make all`

# Installing

**WARNING**: This server contains potential security issues and should not be
installed. If nessessary, steps are provided below.

1. Open `main.c` in a text editor
   1. Change `./keys/` to `#define KEYS_FOLDER "/usr/local/share/progSSHD/keys/"`
   2. Change `static char *default_executable = "./testsh";` to
      `static char *default_executable = "/usr/local/share/progSSHD/testsh";`
2. Recompile code by running `make`
3. Run these commands:
```
sudo mkdir /usr/local/share/progsshd
sudo mkdir /usr/local/share/progsshd/keys
sudo cp -r ./keys/* /usr/local/share/progsshd/keys
sudo cp ./progsshd /usr/local/share/progsshd/progsshd
sudo cp ./testsh /usr/local/share/progsshd/testsh
sudo ln -s /usr/local/share/progsshd/progsshd /usr/local/bin/progsshd
```

# Uninstall

1. Delete the symbolic link:
`sudo rm -f /usr/local/bin/progsshd`

2. Delete the program files:
`sudo rm -rf /usr/local/share/progsshd`

